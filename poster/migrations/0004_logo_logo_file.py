# Generated by Django 3.2.6 on 2021-09-01 12:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('poster', '0003_alter_logo_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='logo',
            name='logo_file',
            field=models.ImageField(null=True, upload_to=''),
        ),
    ]
