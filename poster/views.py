from django.shortcuts import  render, redirect
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User, auth
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages
from .models import Logo,Assets
from django.contrib.auth.decorators import login_required
from .decorators import allowed_users
# Create your views here.

def homepage(request):
    return render(request,'poster/homepage.html')

@csrf_exempt
def register(request):
	if request.method == "POST":
		first_name = request.POST.get("first_name",False)
		last_name = request.POST.get('last_name',False)
		username = request.POST.get('username',False)
		email = request.POST.get('email',False)
		password1 = request.POST.get('password1',False)
		password2 = request.POST.get('password2',False)
		
		if password1==password2:
			if User.objects.filter(username= username).exists():
				messages.info(request,"Username Taken")
				return redirect("register")
			elif User.objects.filter(email=email).exists():
				messages.info(request,"Email ID exists")
				return redirect("register")
			else:
				user = User.objects.create_user(username=username,email=email,password=password1,first_name=first_name,last_name=last_name)
				user.active=True
				user.staff=False
				user.admin=False
				user.save()
				return redirect ("login")
		else:
			messages.info(request,"Passwords not matching")
			return redirect("register")

	else:	
		return render(request,"poster/register.html")


def login_request(request):
	if request.method == "POST":
		inputUsername = request.POST.get("inputUsername", False)
		inputPassword = request.POST.get("inputPassword", False)

		user = auth.authenticate(username=inputUsername,password=inputPassword)
		if user is not None:
			auth.login(request,user)
			return redirect("design")
		else:
			messages.info(request,"Invalid Credentials")
			return redirect("login")
	else:
		return render(request,"poster/login.html")


def design(request):
	username = User.objects.get(id=request.user.id)
	if username==None:
		return render(request,'poster/register.html')
	else:
		logo = request.GET.get("logo")
		#logo_files = request.GET.get("logo_file")
		if logo == None:
			assets = Assets.objects.filter(logo = Logo.objects.filter(user_id=username)[:1].get())
			
		else:
			assets = Assets.objects.filter(logo__name = logo )
			
		
		logos = Logo.objects.filter(user_id = username.id)
		#logo_files = Logo.logo_file.all()
		
		#print(logo_files)
		context = {'logos':logos , "assets" : assets}
		return render(request,'poster/design.html', context)

def logout(request):
	auth.logout(request)
	return redirect("login")

def addAsset(request):
	username = User.objects.get(id=request.user.id)
	if username==None:
		return render(request,'poster/register.html')
	logos = Logo.objects.filter(user_id = username.id)
	if request.method =="POST":
		data = request.POST
		image = request.FILES.get('image')
		logo = Logo.objects.get(id = data['logo'])
		asset = Assets.objects.create(
            logo = logo,
            image = image,
			type = data["type"]
        )
		return redirect('design')
	context = {"logos":logos}
	return render(request,"poster/addAsset.html",context)

def output(request):
	return render(request,"poster/output.html")

def addBrand(request):
	username = User.objects.get(id=request.user.id)
	if username==None:
		return render(request,'poster/register.html')
	logos = Logo.objects.all()
	if request.method == "POST":
		data = request.POST
		if data['logo_new'] != "":
			logo_file = request.FILES.get('logo_file')
			logo = Logo.objects.create(
				name =data['logo_new'], 
				user=username,
				logo_file = logo_file,
				category = data["category"],
				sub_category = data["sub_category"],
				location = data["location"]
				)
			return redirect ('design')
	context = {'logos': logos}
	return render(request,"poster/addBrand.html",context)