from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Logo(models.Model):
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=100,null=False, blank=False)
    logo_file = models.ImageField(null=True, blank= False)
    category = models.CharField(max_length = 200, null= True, blank = True)
    sub_category = models.CharField(max_length = 200, null= True, blank = True)
    location = models.CharField(max_length = 200, null= True, blank = True)

    def __str__(self):
        return self.name

class Assets(models.Model):
    logo = models.ForeignKey(Logo, on_delete=models.SET_NULL, null=True, blank= True)
    image = models.ImageField(null= False, blank= False)
    type = models.CharField(max_length=200, null=True, blank = True)