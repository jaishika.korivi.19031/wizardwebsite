from django.urls import path
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path("", views.homepage, name="homepage"),
    path("register/",views.register,name="register"),
    path("login/", views.login_request, name="login"),
    path("design/", views.design, name="design"),
    path("logout/", views.logout, name="logout"),
    path("addAsset/",views.addAsset, name="addAsset"),
    path("output/",views.output,name="output"),
    path("addBrand/",views.addBrand, name="addBrand"),
]

urlpatterns += staticfiles_urlpatterns()