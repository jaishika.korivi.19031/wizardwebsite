from django.contrib import admin

# Register your models here.
from .models import Logo, Assets

admin.site.register(Logo)
admin.site.register(Assets)
